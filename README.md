# BROKER WEBSITE

To run the project, just type below code in the project directory in the terminal:

`docker-compose up --build`

You need to restart (or close and start) `docker-compose` again when `.env` file is changed.

In any case if change is not reflect please restart `docker-compose`.