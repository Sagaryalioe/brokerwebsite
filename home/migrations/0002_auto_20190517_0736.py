# Generated by Django 2.2.1 on 2019-05-17 07:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='phone_number2',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Next Land Line Number'),
        ),
    ]
