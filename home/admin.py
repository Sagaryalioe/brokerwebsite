from django.contrib import admin

from .models import About, HomeSlider, Contact, Social, Review, NewsLetter, Download, Service, ContactForm

# Register your models here.


class NewsLetterModelAdmin(admin.ModelAdmin):
    list_display = ['email', 'timestamp']
    list_filter = ['timestamp']

    class Meta:
        model = NewsLetter


class ReviewModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'heading', 'avatar', 'timestamp', 'active']
    list_filter = ['timestamp']
    list_editable = ['active']

    class Meta:
        model = Review


class DownloadModelAdmin(admin.ModelAdmin):
    list_display = ['file_name', 'file', 'file_type', 'timestamp']
    list_filter = ['timestamp']

    class Meta:
        model = Download


class ContactFormModelAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'email', 'phone_number', 'timestamp']
    list_filter = ['timestamp']

    class Meta:
        model = ContactForm


admin.site.site_header = "Premier Securities Administration"
admin.site.register(About)
admin.site.register(HomeSlider)
admin.site.register(Contact)
admin.site.register(Social)
admin.site.register(Service)
admin.site.register(Review, ReviewModelAdmin)
admin.site.register(NewsLetter, NewsLetterModelAdmin)
admin.site.register(Download, DownloadModelAdmin)
admin.site.register(ContactForm, ContactFormModelAdmin)
