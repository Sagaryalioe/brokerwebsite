from django.shortcuts import render
from .models import Review, Download, About, Service, HomeSlider
from article.models import News, Notice
from .forms import ContactUsForm
from django.contrib import messages


def home(request):
    review_list = Review.objects.all()
    slider = HomeSlider.objects.all()
    service_list = Service.objects.all()
    latest_news = News.objects.filter(draft=False).order_by("-date")
    flash_notice = Notice.objects.filter(flash_notice=True)
    flash_notice_exists = flash_notice.exists()
    context = {
        "review_list": review_list,
        "latest_news": latest_news and latest_news[0:2],
        "flash_notice": flash_notice and flash_notice[0],
        "flash_notice_exists": flash_notice_exists,
        "service_list": service_list,
        "slider": slider,
        "home": True
    }
    return render(request, 'home/index.html', context)


def about(request):
    context = {
        "about_active": True
    }
    return render(request, 'home/about.html', context)


def contact(request):
    if request.method == 'POST':
        form = ContactUsForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your message have been successfully recieved.')
        else:
            messages.error(
                request, 'Error submitting your message! Please correct errors and Try again.')
    context = {"contact_active": True}
    return render(request, 'home/contact.html', context)


def service(request):
    service_list = Service.objects.all()
    context = {
        "service_list": service_list,
        "service": True
    }
    return render(request, 'home/services.html', context)


def download(request):
    download_list = Download.objects.all()
    context = {
        "download_list": download_list,
        "download": True
    }
    return render(request, 'home/downloads.html', context)
