from django import forms
from .models import NewsLetter, ContactForm


class NewsLetterForm(forms.ModelForm):
    class Meta:
        model = NewsLetter
        fields = ["email"]


class ContactUsForm(forms.ModelForm):
    class Meta:
        model = ContactForm
        fields = [
            "full_name",
            "email",
            "phone_number",
            "address",
            "message"
        ]
