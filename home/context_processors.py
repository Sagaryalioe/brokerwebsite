from home.models import *
from home.forms import NewsLetterForm
from django.contrib import messages


def common(request):
    about = About.objects.all()
    contact = Contact.objects.all()
    social = Social.objects.all()

    context = {
        "about": about and about[0],
        "contact": contact and contact[0],
        "social": social and social[0],
    }
    return context


def newsLetter(request):
    if request.method == 'POST':
        form = NewsLetterForm(request.POST or None, request.FILES or None)
        print(form)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'You have successfully subscribed to our newsletter.')
        else:
            messages.error(
                request, 'Error in subscription. Please correct errors and Try again.')
        context = {
            "form": form,
            "newsLetterContext": True
        }
        return context
    else:
        return {}
