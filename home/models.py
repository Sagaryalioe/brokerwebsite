from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
DOWNLOAD_FILE_TYPE_CHOICES = [
    ('pdf', 'PDF'),
    ('img', 'Image'),
    ('edt', 'Editable Document'),
    ('sht', 'Sheet'),
    ('oth', 'Other'),
]


class About(models.Model):
    about_short = models.TextField("About Short Content")
    about_long = RichTextUploadingField("About Long Content", default=None)

    def __str__(self):
        return self.about_short


class Contact(models.Model):
    company_name = models.CharField("Company Name", max_length=100)
    logo = models.ImageField(upload_to='contact/', default='logo.png')
    phone_number = models.CharField(
        "Land Line Number", max_length=10, blank=True, null=True)
    phone_number2 = models.CharField(
        "Next Land Line Number", max_length=10, blank=True, null=True)
    mobile_number = models.CharField(
        "Mobile Number", max_length=10, blank=True, null=True)
    email = models.EmailField("Email", max_length=254)
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.company_name


class ContactForm(models.Model):
    full_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254)
    address = models.CharField(max_length=250, blank=True, null=True)
    phone_number = models.CharField(max_length=10, blank=True, null=True)
    message = models.TextField()
    timestamp = models.DateTimeField(
        "Contact Date", auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.full_name

    class Meta:
        ordering = ["-timestamp"]
        verbose_name = "Contact Us Form"
        verbose_name_plural = "Contact Us Form"


class Social(models.Model):
    facebook = models.URLField("Facebook Page", max_length=200)
    twitter = models.URLField("Twitter", max_length=200)
    youtube = models.URLField("Youtube Channel", max_length=200)

    def __str__(self):
        return self.facebook


class Review(models.Model):
    name = models.CharField("Full Name", max_length=50)
    heading = models.CharField(max_length=26)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)
    avatar = models.ImageField("Profile Photo", upload_to='reviews/',
                               height_field=None, width_field=None, max_length=None)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class NewsLetter(models.Model):
    email = models.EmailField(max_length=254)
    timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.email


class Service(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    image = models.ImageField(
        "Image", upload_to='services/', blank=True, null=True, default=None)
    icon = models.CharField(blank=True, null=True,
                            max_length=100, help_text="Get relevent icons from https://fontawesome.com/v4.7.0/icons/")

    def __str__(self):
        return self.title


class Download(models.Model):
    file_name = models.CharField("File Name", max_length=250)
    file = models.FileField("File", upload_to='downloads/')
    file_type = models.CharField("File Type",
                                 choices=DOWNLOAD_FILE_TYPE_CHOICES, default='pdf', max_length=3)
    timestamp = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.file_name

    def get_file_type(self):
        if self.file_type == 'pdf':
            return 'PDF'
        elif self.file_type == 'img':
            return 'Image'
        elif self.file_type == 'edt':
            return 'Editable Document'
        elif self.file_type == 'sht':
            return 'Spread Sheet'
        else:
            return 'Other'


class HomeSlider(models.Model):
    caption = models.CharField(max_length=150)
    sub_caption = models.CharField(max_length=400)
    image = models.ImageField(upload_to='sliders/')

    def __str__(self):
        return self.caption
