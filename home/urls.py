from django.urls import path
from .views import home, contact, service, about, download

app_name = "home"
urlpatterns = [
    path('', home, name="home"),
    path('about/', about, name="about"),
    path('contact/', contact, name="contact"),
    path('services/', service, name="service"),
    path('downloads/', download, name="download"),
]
