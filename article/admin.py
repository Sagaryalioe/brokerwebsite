from django.contrib import admin
from .models import News, Notice, Tag
# Register your models here.


class NewsModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'auther', 'get_tags', 'date', 'draft']
    list_filter = ['date', 'tag', 'auther']
    list_editable = ['draft']

    class Meta:
        model = News


class NoticeModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'date', 'flash_notice']
    list_filter = ['date', 'flash_notice']
    list_editable = ['flash_notice']

    class Meta:
        model = Notice


admin.site.register(News, NewsModelAdmin)
admin.site.register(Notice, NoticeModelAdmin)
admin.site.register(Tag)
