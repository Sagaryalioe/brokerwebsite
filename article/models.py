from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.utils.text import slugify
from django.db.models.signals import pre_save
from django.urls import reverse
from django.utils import timezone
# Create your models here.


class Notice(models.Model):
    title = models.CharField(max_length=250)
    slug = models.SlugField(unique=True, blank=True, null=True)
    image = models.ImageField(upload_to='notices/', blank=True, null=True)
    content = models.TextField()
    date = models.DateField(
        auto_now=False, auto_now_add=False, default=timezone.now)
    flash_notice = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("article:noticeDetail", kwargs={"slug": self.slug})


class Tag(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("article:tagNews", kwargs={"name": self.name})


class News(models.Model):
    title = models.CharField(max_length=250)
    auther = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    slug = models.SlugField(unique=True, blank=True, null=True)
    image = models.ImageField(
        upload_to='news/', height_field=None, width_field=None, max_length=None)
    tag = models.ManyToManyField(Tag)
    content = RichTextUploadingField(default=None)
    date = models.DateField(
        auto_now=False, auto_now_add=False, default=timezone.now)
    draft = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    def get_tags(self):
        return "\n, ".join([c.name for c in self.tag.all()])

    def get_absolute_url(self):
        return reverse("article:articleDetail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-date"]
        verbose_name = "News"
        verbose_name_plural = "News"


def check_exists(model, slug):
    qs = model.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    return exists


def create_slug(model, instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug

    qs = model.objects.filter(slug=slug).order_by("-id")
    exists = check_exists(model, slug)
    if exists:
        new_slug = "%s-%s" % (slug, qs.first().id)
        return create_slug(model, instance, new_slug=new_slug)
    return slug


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    instance.slug = create_slug(sender, instance)
    # do other stuffs here


pre_save.connect(pre_save_post_receiver, sender=News)
pre_save.connect(pre_save_post_receiver, sender=Notice)
