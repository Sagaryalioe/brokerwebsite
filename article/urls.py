from django.urls import path
from article.views import article, article_detail, notice, tag_article, notice_detail

app_name = "article"

urlpatterns = [
    path('', article, name="articles"),
    path('notices/', notice, name="notices"),
    path('<slug:slug>', article_detail, name="articleDetail"),
    path('notice/<slug:slug>', notice_detail, name="noticeDetail"),
    path('tag/<str:name>', tag_article, name="tagNews")

]
