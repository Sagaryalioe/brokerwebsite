from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from .models import News, Notice, Tag
# Create your views here.


def article(request):
    tag_list = Tag.objects.all()
    news = News.objects.filter(draft=False)
    paginator = Paginator(news, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    news_list = paginator.get_page(page)
    recent_news = News.objects.all().order_by('-date')
    flash_notice = Notice.objects.filter(flash_notice=True)
    context = {
        "tag_list": tag_list,
        "news_list": news_list,
        "recent_news": recent_news and recent_news[0:3],
        "flash_notice": flash_notice and flash_notice[0],
        "news": True
    }
    return render(request, 'article/articles.html', context)


def tag_article(request, name=None):
    news = News.objects.filter(tag__name=name).filter(draft=False)
    paginator = Paginator(news, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    news_list = paginator.get_page(page)
    tag_list = Tag.objects.all()
    recent_news = News.objects.all().order_by('-date')
    context = {
        "news_list": news_list,
        "tag_list": tag_list,
        "recent_news": recent_news and recent_news[0:3],
        "news": True
    }
    return render(request, 'article/articles.html', context)


def notice(request):
    notice_list = Notice.objects.all()
    context = {
        "notice_list": notice_list,
        "news": True

    }
    return render(request, 'article/notices.html', context)


def article_detail(request, slug=None):
    instance = get_object_or_404(News, slug=slug)
    recent_news = News.objects.filter(draft=False).order_by('-date')
    tag_list = Tag.objects.all()
    context = {
        "instance": instance,
        "tag_list": tag_list,
        "recent_news": recent_news,
        "news": True
    }
    return render(request, 'article/article-detail.html', context)


def notice_detail(request, slug=None):
    instance = get_object_or_404(Notice, slug=slug)
    context = {
        "instance": instance,
        "news": True
    }
    return render(request, 'article/notice-detail.html', context)
