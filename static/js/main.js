$(document).ready(function() {
  var animate = new Animate({
    animatedClass: "js-animated",
    offset: [0.1, 0.2],
    delay: 0,
    remove: true, // remove class after completation
    scrolled: false, // animate after element has beed scrolled
    target: "[data-animate]",
    reverse: false,
    disableFilter: null, // function to determine whether Animate should animate elements.
    onLoad: true,
    onScroll: true,
    onResize: false,
    callbackOnInit: function() {
      console.log("Initialised");
    },
    callbackOnInView: function(element) {
      var animationType = element
        .getAttribute("data-animation-classes")
        .replace("animated", "")
        .trim();
      console.log(animationType + " in view.");
    },
    callbackOnAnimate: function(element) {
      console.log(element);
    }
  });

  animate.init();
});
