#!/bin/sh

# any future command that fails will exit the script
set -e

# add ssh key stored in PRIVATE_KEY variable to the agent store
eval $(ssh-agent -s)

##
## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
## We're using tr to fix line endings which makes ed25519 keys work
## without extra base64 encoding.
## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
##
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null


mkdir -p ~/.ssh/
chmod 700 ~/.ssh

echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Not so good method
# # Lets write the public key of our aws instance or any other server
# echo -e "$PRIVATE_KEY" > ~/.ssh/id_rsa
# chmod 600 ~/.ssh/id_rsa

# ** Alternative approach
# # eval $(ssh-agent -s)
# # echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
# ** End of alternative approach

# disable the host key checking.
# this method might allow MITM attack
# check the docs: https://docs.gitlab.com/ce/ci/ssh_keys/README.html
# touch ~/.ssh/config
# echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

ssh ubuntu@$DEPLOY_SERVER 'bash -s' < ./deploy/serverTasks.sh $RELEASE_IMAGE $IMAGE_NAME