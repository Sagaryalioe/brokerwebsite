# Pull base image
FROM python:3.7

# Set Environment Variables
# PYTHONUNBUFFERED ensures our console output looks familiar 
# and is not buffered by Docker, which we don’t want. 
ENV PYTHONUNBUFFERED 1

# If you prefer miniconda:
#FROM continuumio/miniconda3

# Set working directory
WORKDIR /backend

# Copy only the requirements.txt    
ADD ./requirements.txt ./

# Install dependencies
RUN pip install -r requirements.txt

# Copy whole project
ADD . /backend

# Keep the port open inside docker container
EXPOSE 8000

CMD ./manage.py migrate && \
    ./manage.py collectstatic --noinput && \
    gunicorn --bind 0:8000 --access-logfile - brokerWebsite.wsgi:application
