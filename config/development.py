import os
from distutils.util import strtobool

from config.common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = strtobool(os.getenv('DJANGO_DEBUG', 'yes'))

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('POSTGRES_DB', 'postgres'),
        'USER': os.getenv('POSTGRES_USER', 'postgres'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', 'asdf1234'),
        # The host is the name of database service in docker-compose.yml
        'HOST': os.getenv('POSTGRES_HOST', 'db'),
        'PORT': os.getenv('POSTGRES_PORT', '5432')
    }
}

# For development - Don't use STATIC_ROOT
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static/'),
)
