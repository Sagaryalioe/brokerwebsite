import os
from distutils.util import strtobool

from config.common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = strtobool(os.getenv('DJANGO_DEBUG', 'no'))

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('POSTGRES_DB'),
        'USER': os.getenv('POSTGRES_USER'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
        # The host is the name of database service in docker-compose.yml
        'HOST': os.getenv('POSTGRES_HOST'),
        'PORT': os.getenv('POSTGRES_PORT')
    }
}
# Used during `collectstatic` to copy static files to given directory below
# This directory can be used to serve static contents in production
# using Nginx, Apache
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

